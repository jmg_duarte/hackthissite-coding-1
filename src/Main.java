import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

	private static Map<String, String> wordList;
	
	public static void main(String[] args) {
		Scanner textInput = new Scanner(System.in);
		String filePath = getFilePath(textInput);
		wordList = new HashMap<String, String>();
		
		try {
			File file = new File(filePath);
			Scanner fileReader = new Scanner(new FileReader(file));
			readFile(fileReader);
			fileReader.close();
			
			textInput.reset();
			unscrambleWord(textInput);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private static String getFilePath(Scanner textInput){
		System.out.println("Insert the wordlist location:");
		String filePath = textInput.nextLine();
		return filePath;
	}
	
	private static void readFile(Scanner fileReader) throws IOException{
		System.out.println("File reader start.");
		String word, key = "";
		while(fileReader.hasNextLine()){
			word = fileReader.nextLine();
			char[] keyWord = word.toCharArray();
			Arrays.sort(keyWord);
			key = Arrays.toString(keyWord);
			wordList.put(key, word);
		}
		System.out.println("File read.");
	}
	
	private static void unscrambleWord(Scanner userInput){
		System.out.println("Insert scrambled word:");
		String scrambledWord = "", output = "";
//		while (!scrambledWord.equals("quit")) { // check this condition
		for(int i = 0; i < 10; i++){
			scrambledWord = userInput.nextLine();
			userInput.nextLine();
			char[] scrambledToKey = scrambledWord.toCharArray();
			Arrays.sort(scrambledToKey);
			scrambledWord = Arrays.toString(scrambledToKey);
			if (wordList.containsKey(scrambledWord)) {
//				System.out.println(wordList.get(scrambledWord));
				output += wordList.get(scrambledWord) + ",";
				System.out.println(output);
			} else {
				System.out.println("Word does not exist.");
			} 
		}
		userInput.close();
	}
}
